$(document).ready(function() {

    // extract restaurant identifier
    var current_url = document.location.href;
    var index_of_biz = current_url.indexOf('biz/');
    var restaurant_identifier = current_url.substr(index_of_biz + 4);

    var loading_section = '<div class="loading-div"><img src="http://i.stack.imgur.com/FhHRx.gif" /></div>';

    $('.review-highlights').before('<div class="dish-rater island"><button type="button" id="recommend-dish">Recommend</button><div class="dish-rater-content"><br />Click the button above to get dish recommendations.</div></div><br />');

    // $(document).on({
    //     ajaxStart: function() { $('.dish-rater').addClass("loading"); },
    //     ajaxStop: function() { $('.dish-rater').removeClass("loading"); }    
    // });

    $("#recommend-dish").click(function() {
        $('.dish-rater').html(loading_section)

        $.ajax({
          url: "http://127.0.0.1:8000/rate/" + restaurant_identifier,
          timeout: 120000
        }).always(function(response) {
            var response_copy = response;

            // the returned json will be fucked-up, so fix it first
            // response_copy = response_copy.replace(/\",/g, '":');
            // response_copy = response_copy.replace(/\[/g, '{');
            // response_copy = response_copy.replace(/\]/g, '}');
            // response_copy = '[' + response_copy.substring(1);
            // response_copy = response_copy.substring(0, response_copy.length - 1) + ']';

            // parse json string to obj
            // var jsonObj = $.parseJSON(response_copy);
            var jsonObj = $.parseJSON(response_copy);

            var dishHtml = "<b>Recommended dishes:</b><br /><br />";

            $.each(jsonObj, function(index, obj) {
                dishHtml += obj.name + "<br />";
            });

            $('.dish-rater').html(dishHtml);

            // console.log(jsonObj);

            // $.each(jsonObj, function(key, value) {
            //     alert(key);
            // });
            // alert(response_copy);
        });
    });
});


