from django.http import HttpResponse

import re
import requests
from bs4 import BeautifulSoup
import html
import json
import pprint


class review_data(object):

    def __init__(self, reviewCount, reviewText):
        self.reviewCount = reviewCount
        self.reviewText = reviewText


class menu_data(object):

    def __init__(self, itemCount, menuText):
        self.itemCount = itemCount
        self.menuText = menuText


def scrape_review(restaurant_name):
    final_json = ""
    review_list = []
    review_count = 0

    # for i in range(0, 1):
    url = "https://www.yelp.com/biz/" + \
        restaurant_name + "?start=" + str(0 * 20)
    response = requests.get(url)
    response_content = response.content

    soup = BeautifulSoup(response_content)
    reviews = soup.find('div', attrs={'class': 'review-list'})

    for review in reviews.find_all('p', attrs={'lang': 'en'}):
        review_list.append(review_data(review_count, html.unescape(
            review.prettify().replace('<br />', '').strip())))
        review_count += 1

    json_review_list = []

    for item in review_list:
        json_review_list.append(json.dumps(item.__dict__))

    final_json = json.dumps(json_review_list)

    return final_json


def scrape_menu(restaurant_name):
    menu_list = []
    item_count = 0
    final_json = ""

    url = "https://www.yelp.com/menu/" + restaurant_name
    response = requests.get(url)
    response_content = response.content

    soup = BeautifulSoup(response_content)
    items = soup.find('div', attrs={'class': 'menu-sections'})
    # print items
    for item in items.find_all('h4'):
        link_removed = re.sub(r'</?a(?:(?= )[^>]*)?>', '', item.prettify())
        link_removed = link_removed.replace('\n', '')
        link_removed = link_removed.replace('<h4>', '')
        link_removed = link_removed.replace('</h4>', '')
        link_removed = link_removed.strip()

        menu_list.append(menu_data(item_count, link_removed))
        item_count += 1

    json_menu_list = []

    for item in menu_list:
        json_menu_list.append(json.dumps(item.__dict__))

    final_json = json.dumps(json_menu_list)

    return final_json


# def scrape(request, restaurant_name):
#     return HttpResponse(scrape_review(restaurant_name))
