from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<restaurant_name>(.*?))/$', views.scrape, name='restaurant_name'),
]