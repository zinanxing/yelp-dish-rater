import requests
from BeautifulSoup import BeautifulSoup
import HTMLParser
import json
import pprint

class review_data(object):
        def __init__(self,reviewCount,reviewText):
            self.reviewCount = reviewCount
            self.reviewText = reviewText

def scrape_review(restaurant_name):
    html_parser = HTMLParser.HTMLParser()
    final_json = ""
    review_list = []
    review_count = 0

    for i in range(0,1):
        url= "https://www.yelp.com/biz/" + restaurant_name + "?start=" + str(i*20)
        response = requests.get(url)
        html = response.content

        soup = BeautifulSoup(html)
        reviews = soup.find('div', attrs={'class': 'review-list'})
        
        for review in reviews.findAll('p', attrs={'lang': 'en'}):
            review_list.append(review_data(review_count,html_parser.unescape(review.prettify().replace('<br />','').decode('utf8'))))
            review_count += 1
        	
    for review in review_list:
    	final_json += json.dumps(review.__dict__)

    return final_json
