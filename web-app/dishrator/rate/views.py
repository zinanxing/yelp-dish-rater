from django.http import HttpResponse

import time
import operator
import numpy
import requests
from scrape import views
import html
from nltk import tokenize

import json


class dish(object):

    def __init__(self, name, score):
        self.name = name
        self.score = score


def index(request, restaurant_name):
    menu_items = json.loads(views.scrape_menu(restaurant_name))
    reviews = json.loads(views.scrape_review(restaurant_name))

    dish_score_set_map = {}
    dish_score_map = {}

    # init dish score set map
    for menu_item in menu_items:
        menu_item_json = json.loads(menu_item)
        menu_item_name = menu_item_json['menuText']
        dish_score_set_map[menu_item_name] = set()

    sentences = []

    # for review in reviews:
    for review in reviews:
        review_json = json.loads(review)
        review_text = review_json['reviewText']

        # break review down to sentences
        sentences = break_to_sentences(review_text)

        collected_labels = []

        # need a counter to not exceed API limit
        counter = 0

        for sentence in sentences:
            counter += 1
            if(counter > 50):
                break

            # get sentiment response
            # option 1 - direct
            payload = {'text': html.escape(sentence)}
            response = requests.post(
                'http://text-processing.com/api/sentiment/', data=payload)
            json_response = response.json()

            # option 2 - mashape
            # payload = {'text': html.escape(sentence)}
            # headers = {
            #     "X-Mashape-Key": "ZSFCSPVxkImshL4jVTf5sOe84dwlp1LbTSBjsnjOBLKnvMX0bg",
            #     "Content-Type": "application/x-www-form-urlencoded",
            #     "Accept": "application/json"
            # }
            # response = requests.post(
            #     'https://japerk-text-processing.p.mashape.com/sentiment', data=payload, headers=headers)
            # json_response = response.json()

            # # sleep so not to overwhelm the API
            # time.sleep(1)

            # calc sentiment score
            sentiment_score = 0
            if json_response['label'] == 'pos':
                sentiment_score += float(json_response['probability']['pos'])
            elif json_response['label'] == 'neg':
                sentiment_score -= float(json_response['probability']['neg'])

            # detect dish name appearences in sentence, assign scores
            for dish_name in dish_score_set_map:
                if dish_name.lower() in sentence.lower():
                    dish_score_set_map[dish_name].add(sentiment_score)

    # calc final sentiment score for each dish
    for dish_name, score_set in dish_score_set_map.items():
        score_list = list(score_set)
        if(len(score_list) > 0):
            dish_score_map[dish_name] = numpy.mean(score_list)
        else:
            dish_score_map[dish_name] = -1

    # get the top rated ones
    sorted_dict_as_list = sorted(
        dish_score_map.items(), key=operator.itemgetter(1), reverse=True)

    # dish object list
    final_list = []

    for tup in sorted_dict_as_list[:3]:
        final_list.append(dish(tup[0], tup[1]))

    # final_json = json.dumps(sorted_dict_as_list[:3])
    final_json = json.dumps([ob.__dict__ for ob in final_list])

    return HttpResponse(final_json)

    # experiment
        # payload = {'text': 'great movie', 'language': 'english'}
        # headers = {
        #     "X-Mashape-Key": "7NnOiHsWTymsh94u97eyrtuJAU3qp1NNVTLjsnMeBkVynnYBza",
        #     "Content-Type": "application/x-www-form-urlencoded",
        #     "Accept": "application/json"
        # }
        # response = requests.post(
        #     'https://japerk-text-processing.p.mashape.com/sentiment/', data=payload, headers=headers)
        # json_response = response.json()

        # return HttpResponse(json_response['probability']['pos'])

    # time.sleep(3)
    # return HttpResponse('[{"name": "Spicy Readhead", "score":0.637}, {"name":"Madrid Moon", "score":0.324}, {"name":"Miso Soup", "score":0.296}]')
    # return HttpResponse('sdf')

# break string to sentences
def break_to_sentences(str):
    return tokenize.sent_tokenize(str)
