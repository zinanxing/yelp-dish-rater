from django.conf.urls import url
from scrape import views

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<restaurant_name>(.*?))/$', views.index, name='restaurant_name'),
]