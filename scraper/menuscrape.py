import requests
from BeautifulSoup import BeautifulSoup
import HTMLParser
import json
import pprint
import re

html_parser = HTMLParser.HTMLParser()
menu_list = []
item_count = 0
class menu_data(object):
	def __init__(self,itemCount,menuText):
		self.itemCount = itemCount
		self.menuText = menuText

url = "https://www.yelp.com/menu/japonessa-seattle/main-menu"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
items = soup.find('div', attrs={'class': 'menu-sections'})
#print items
for item in items.findAll('h4'):
	link_removed = re.sub(r'</?a(?:(?= )[^>]*)?>','',item.prettify())
	link_removed = link_removed.replace('\n','')
	link_removed = link_removed.replace('<h4>','')
	link_removed = link_removed.replace('</h4>','')
	link_removed = link_removed.lstrip()

	menu_list.append(menu_data(item_count,link_removed))
	item_count += 1

for item in menu_list:
	print json.dumps(item.__dict__)