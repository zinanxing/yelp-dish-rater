import requests
from BeautifulSoup import BeautifulSoup
import HTMLParser
import json
import pprint

html_parser = HTMLParser.HTMLParser()
review_list = []
review_count = 0
class review_data(object):
    def __init__(self,reviewCount,reviewText):
    	self.reviewCount = reviewCount
    	self.reviewText = reviewText

for i in range(0,(3000/20)):
    url= "https://www.yelp.com/biz/japonessa-seattle?start=" + str(i*20)
    response = requests.get(url)
    html = response.content

    soup = BeautifulSoup(html)
    reviews = soup.find('div', attrs={'class': 'review-list'})
    
    for review in reviews.findAll('p', attrs={'lang': 'en'}):
        review_list.append(review_data(review_count,html_parser.unescape(review.prettify().replace('<br />','').decode('utf8'))))
        review_count += 1
    	
for review in review_list:
	print json.dumps(review.__dict__)
